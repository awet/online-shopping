$(document).ready(function()
{
	$('#cmb_size').change(function()
	{
		
		if(this.value > 0)
		{
		  $('#order-type').html('Order Type : Ready Made , size available in stock.');
		  $('#order-duration').html('Ready For Shippment');	
		  $('#order-shippment').html('Shipping : 3-4 DAYS');	
		} else if(this.value == 0 ){
		  $('#order-type').html('Order Type : made for order');
		  $('#order-duration').html('Proccessing time : 3 - 4 WEEKS');	
		  $('#order-shippment').html('Shipping : 3-4 DAYS');		
		} else if(this.value == -1 ){
		  $('#order-type').html('please select size');	
		  $('#order-duration').html('');	
		  $('#order-shippment').html('');	
		}	
	});
	
	$("form").submit(function() {
		if($('#cmb_size option:selected').val() == -1)
		{
			$('#add-error').html('* Please Select size !');
			alert('Select Size from the available choice !');
			$('#cmb_size').focus();
			return false;
		}
		
	});
	
});	