<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
     public function indexAction(Request $request)
     {    
         return $this->render('index.html.twig');
     }
     
     /**
     * @Route("/about", name="about")
     */
     public function aboutAction(Request $request)
     {
         return $this->render('home/about.html.twig');
     }
     
     /**
     * @Route("/customer-service", name="customer_service")
     */
     public function customerServiceAction(Request $request)
     {
         return $this->render('home/customer-service.html.twig');
     }
     
     /**
     * @Route("/orders-and-returns", name="orders_returns")
     */
     public function ordersAndReturnsAction(Request $request)
     {
         return $this->render('home/orders-returns.html.twig');
     }
     
     /**
     * @Route("/privacy-and-policy", name="privacy_policy")
     */
     public function privacyAndPolicyAction(Request $request)
     {
         return $this->render('home/privacy-policy.html.twig');
     }
     
}
