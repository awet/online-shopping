<?php 

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Braintree;
use Braintree\Configuration as Braintree_Configuration;
use Braintree\Transaction as Braintree_Transaction;
use Braintree\ClientToken as Braintree_ClientToken;
/**
 * Payment Gateway
 * 
 * @Route("Gateway")
 */
class GatewayController extends Controller
{
 
    /**
     * @Route("/payment-method", name="payment_method")
     */
    public function braintreePaymentAction(Request $request)
    {
           //used for setting session id in cart entity    
           $session = $request->getSession();
           $session->start();    
        
           $em = $this->getDoctrine()->getManager(); 
           //lets grap logged in user if available 
           $user = $this->get('security.token_storage')->getToken()->getUser();
           if($user == 'anon.')   //anonymouse login
           {
               $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
               //grap the cart for current user  - by session id  
               $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           else {
              $user = $this->getUser();
              //grap the cart for current user  - by userId
              $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           }  
             
           $items = $cart->getItems();
           $total = 0;
             
           foreach($items as $item)
           { 
              $total += floatval($item->getQty() * $item->getUnitPrice());
           } 
            
           Braintree_Configuration::environment('sandbox');
           Braintree_Configuration::merchantId('nydfhwzjsy7w2s5d');
           Braintree_Configuration::publicKey('dnp37crsspby5m48');
           Braintree_Configuration::privateKey('e04adfe2ec3d718f3044dc02a7e221b2');
           
           try
           {
              $clientToken = Braintree_ClientToken::generate();
               
              return $this->render('gateway/payment.html.twig', array(
                'lable' => "braintree Gatway",
                'client_token' => $clientToken,
                'items' => $items,
                'total' => $total,
              ));
           } 
           catch(\Exception $e){
              echo("bashnak");
              exit;
           }
           
    }
    
     
     /**
      * @Route("/sell", name="sell")    
      */
     public function setNonce(Request $request)
     {
         //used for setting session id in cart entity    
         $session = $request->getSession();
         $session->start();    
          
         $nonce = $request->get('payment_nonce');
         $nonce = 'fake-valid-nonce';
         $session->set('nonce', $nonce);
         
         return new JsonResponse($nonce);
         //$this->get('session')->set('nonce', $nonce);
           
     }
     
    /**
     * @Route("/payment-method-2", name="payment_method_2")
     */
    public function _2checkOutPaymentAction(Request $request)
    {  
        $total = 200;
            
        return $this->render('gateway/payment_2.html.twig', array(
                'total' => $total,
              ));    
           
    }     
}
