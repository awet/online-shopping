<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SalesOrder;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Braintree;
use Braintree\Configuration as Braintree_Configuration;
use Braintree\Transaction as Braintree_Transaction;
use Braintree\ClientToken as Braintree_ClientToken;
use Braintree\Customer as Braintree_Customer;
use Braintree\Exception;
use Twocheckout;
use Twocheckout_Charge;
use Twocheckout_Error;

/**
 * order controller.
 *
 * @Route("order")
 */
class OrderController extends Controller 
{
    /**
     * @Route("/review-order", name="review_order")
     */
    public function indexAction()
    {
         if($user = $this->getUser())
         {
             $em = $this->getDoctrine()->getManager();   
             
             $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));
             $items = $cart->getItems();
             $total = 0;
             
             foreach($items as $item)
             { 
                 $total += floatval($item->getQty() * $item->getUnitPrice());
             }
             
             return $this->render('order/review_order.html.twig', array(
              'user' => $user,
              'items' => $items,
              'total' => $total,
             ));
         }
         
       return $this->redirectToRoute('login');  
    }
    

    
    /**
     * @Route("/place-order", name="place_order")
     */
    public function orderAction(Request $request, \Swift_Mailer $mailer)
    {
                            
           //used for setting session id in cart entity    
           $session = $request->getSession();
           $session->start();   
        
           //get total price      
           $em = $this->getDoctrine()->getManager();   
           
           //lets grap logged in user if available 
           $user = $this->get('security.token_storage')->getToken()->getUser();
           
           if($user == 'anon.')   //anonymouse login
           {
               $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
               //grap the cart for current user  - by session id  
               $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           else {
              $user = $this->getUser();
              //grap the cart for current user  - by userId
              $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           
           if($cart)
           {
               $items = $cart->getItems();
               $total = 0;
               
               foreach($items as $item)
               { 
                   $total += floatval($item->getQty() * $item->getUnitPrice());
               }
     
               //lets grap billingShipping by cart_id before the cart removed 
                $billingShipping = $em->getRepository('AppBundle:BillingShipping')->findOneBy(array('cartId' => $cart->getId())); 
                
                              
               //select payment getway type
               if($this->getParameter('payment_gateway_type')=="2CO-API")
               {
                     Twocheckout::privateKey($this->getParameter('twocheckout_privateKey'));
                     Twocheckout::sellerId($this->getParameter('twocheckout_sellerId'));
                     Twocheckout::sandbox($this->getParameter('twocheckout_sandbox'));  
                    
                      try {
                          $charge = Twocheckout_Charge::auth(array(
                              "merchantOrderId" => $billingShipping->getId(),
                              "token"      => $request->get('token'),
                              "currency"   => 'USD',
                              "total"      => $total,
                              "billingAddr" => array(
                                  "name" => $billingShipping->getFirstName() . ' ' . $billingShipping->getLastName(),
                                  "addrLine1" => $billingShipping->getApartmentNo() . ' ' . $billingShipping->getStreetAddress(),
                                  "city" => $billingShipping->getCity(),
                                  "state" => $billingShipping->getState(),
                                  "zipCode" => $billingShipping->getPostCode(),
                                  "country" => $billingShipping->getCountry(),
                                  "email" => $billingShipping->getEmail(),
                                  "phoneNumber" => $billingShipping->getPhone()
                              )
                          ));
                     
                          if ($charge['response']['responseCode'] == 'APPROVED') {
                              //echo "Thanks for your Order!";
                              $sales_no = $charge['response']['orderNumber'];
                              
                              //Prepare the order --   GOD I AM SO HAPPY YOU ARE MY ONLY FATHER AND YOU ARE THE MEANING FOR MY LIFE 
                              
                              //create new order 
                              $order = new SalesOrder();
                              $now = new \DateTime();
                              $shipping_rate = floatval($billingShipping->getShippingRate());  //grap the shipping rate from the billing shipping entity
                              
                              $order->setBillingShipping($billingShipping);  //FK
                              $order->setSalesNo($sales_no);   // sales no / order no refrenced on the payment gateway 
                              $order->setUserId($user->getId());
                              $order->setItemsPrice($total);
                              $order->setShipmentPrice($shipping_rate);
                              $order->setTotalPrice(floatval($total + $shipping_rate));
                              $order->setStatus($order::ORDER_PENDING);
                              $order->setCreatedAt($now);
                              $order->setModifiedAt($now);
                              //THANK YOU JESUS THANK YOU FATHER 
       
                              //create order item for each cartitme and remove from cart item 
                              foreach($items as $item)
                              {
                                  $orderItem = new OrderItem();
                                  $orderItem->setOrder($order);
                                  $orderItem->setOrderId($order->getId());
                                  $orderItem->setProductId($item->getProduct()->getId());
                                  $orderItem->setTitle($item->getProduct()->getTitle());
                                  $orderItem->setQty($item->getQty());
                                  $orderItem->setUnitPrice($item->getUnitPrice());
                                  $orderItem->setTotalPrice($item->getQty() * $item->getUnitPrice());
                                  $orderItem->setCreatedAt($now);
                                  $orderItem->setModifiedAt($now);  
                                  
                                  $em->persist($orderItem);
                                  //$em->remove($item);  //remove from the cartitem from the user              
                              }
                              
                              $em->persist($order);
                              $em->remove($cart);
                              $em->flush();
                              $last_order = $order->getId();   
 
                              //lets get base url for image display in email 
                              $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();


                              //let send order confimation email
                              $message = (new \Swift_Message('Order confirmation - '. $charge['response']['orderNumber'] . '-' . $last_order . ' God I will see your glory '))
                                   ->setFrom(['awet@awetshop.com' => 'awetshop'])
                                   ->setTo($billingShipping->getEmail())
                                   ->setCc($billingShipping->getEmail())
                                   ->setBcc('avvvet@gmail.com')
                                   ->setBody(
                                       $this->renderView(
                                           'email/order_confirmation.html.twig',
                                            array(
                                              "name" => $billingShipping->getFirstName() . ' ' . $billingShipping->getLastName(),
                                              "addrLine1" => $billingShipping->getApartmentNo() . ' ' . $billingShipping->getStreetAddress(),
                                              "city" => $billingShipping->getCity(),
                                              "state" => $billingShipping->getState(),
                                              "zipCode" => $billingShipping->getPostCode(),
                                              "country" => $billingShipping->getCountry(),
                                              "email" => $billingShipping->getEmail(),
                                              "phoneNumber" => $billingShipping->getPhone(),
                                              "order" => $charge['response']['orderNumber'],
                                              "items" => $items,
                                              "total" => $total,
                                              "baseurl" => $baseurl
                                            )
                                       ),
                                       'text/html'
                                   );
                              
                              if (!$mailer->send($message, $failures))
                              {
                                dump($failures) ;                           
                                exit;  
                              }
                              
                              // flash message 
                              $request->getSession()
                                      ->getFlashBag()
                                      ->add('success', 'Thank you - we received your order successfully ! email has been sent to you , your order reference is ' . $charge['response']['orderNumber'] . '-' . $last_order . '.');
                      
                              return $this->redirectToRoute('home');                         
                          }
     
                      } catch (\Twocheckout_Error $e) {
                          
                          if($e->getCode() == 403)
                          {
                              $e = "It looks you have limited connection - we could't connect to our server !";
                          } elseif($e->getCode() == 300)
                          {
                              $e = $e->getMessage() . " Please submit your payment method ";
                          }
                          elseif($e->getCode() == 602)
                          {
                              $e = $e->getMessage();
                          } else {
                              $e = $e->getMessage();
                          }

                          
                          return $this->render('gateway/payment_3.html.twig', array(
                                'sid' => '901362471',
                                'mode' => '2CO',
                                'items' => $items,
                                'total' => $total,
                                'error' => $e  
                          ));
             
                          print_r($e->getMessage());
                      }               
               } 
               elseif($this->getParameter('payment_gateway_type')=="braintree")
               {
                      //let process the payment with the nonce
                      Braintree_Configuration::environment($this->getParameter('braintree_env'));
                      Braintree_Configuration::merchantId($this->getParameter('braintree_merchantId'));
                      Braintree_Configuration::publicKey($this->getParameter('braintree_pk'));
                      Braintree_Configuration::privateKey($this->getParameter('braintree_pvk'));        
                      
                      //$nonce = $this->get('session')->get('nonce');
                      $nonce = $session->get('nonce');
                      
                      //lets check if the customer existes
                      $customer_flag = false;
                      
                      try
                      {
                         $customer = Braintree_Customer::find($user->getId());
                         $customer_flag = true;
                      } 
                      catch(\Exception $e)
                      {
                          $customer_flag = false;
                      }
                      
                      
                      if($customer_flag)  //customer existes 
                      {
                         $result = Braintree_Transaction::sale([
                           'amount' => floatval($total),
                           'paymentMethodNonce' => $nonce,
                           'customerId' => $user->getId(),
                           'options' => [ 
                                          'submitForSettlement' => true,
                                          'storeInVaultOnSuccess' => true,
                           ]
                                          
                         ]);   
                      }
                      else {  //new customer 
                          $result = Braintree_Transaction::sale([
                           'amount' => floatval($total),
                           'paymentMethodNonce' => $nonce,
                           'customer' => [
                                           'id' => $user->getId(),
                                           'firstName' => $user->getFirstName(),
                           ],
                           'options' => [ 
                                          'submitForSettlement' => true,
                                          'storeInVaultOnSuccess' => true,
                           ]
                                          
                         ]);  
                      }
               } //endif -payment gateway
                
           } 
          
           // if no no cart item  
           $request->getSession()
                   ->getFlashBag()
                   ->add('success', 'Please add items to your cart ! ');
       
           return $this->redirectToRoute('home');                 
          
            
    }
    
}