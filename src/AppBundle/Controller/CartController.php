<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cart controller.
 *
 * @Route("cart")
 */
class CartController extends Controller 
{
    /**
     * @Route("/add-cart/{product_id}", name="add_cart")
     */
    public function addCartAction(Request $request, $product_id)
    {
           //used for setting session id in cart entity    
           $session = $request->getSession();
           $session->start();    
           
           $em = $this->getDoctrine()->getManager();
           $now = new \DateTime();
           
           //grap the selected product to for add cart
           $product = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $product_id));
           
           //lets grap logged in user if available 
           $user = $this->get('security.token_storage')->getToken()->getUser();
           
           if($user == 'anon.')   //anonymouse login Thank You Father
           {
               $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
               //grap the cart for current user  - by session id  
               $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           else {
              $user = $this->getUser();
              //grap the cart for current user  - by userId
              $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           
           
           //if there is no cart create it 
           if(!$cart)
           {
               $cart = new Cart();
               $cart->setUserId($user->getId());
               $cart->setSessionId($session->getId());
               $cart->setCreatedAt($now);
               $cart->setModifiedAt($now);
           } else {
               $cart->setModifiedAt($now);
           }
           
           $em->persist($cart);
           $em->flush();
           
           //grap the possibly exiting cart item
           $cartItem = $em->getRepository('AppBundle:CartItem')->findOneBy(array('cartId' => $cart->getId(), 'productId' => $product->getId()));
           if($cartItem)
           {
               //if user cart and product exists - it means the user is adding similar product so lets update it 
               //$cartItem->setQty($cartItem->getQty() + 1);$items = $request->get('item');
               $cartItem->setQty($request->get('qty'));
               $cartItem->setSize($request->get('size'));
               $cartItem->setModifiedAt($now);
           } else {
               //cart item does not exist add new one 
               $cartItem = new CartItem();
               $cartItem->setCartId($cart->getId());
               $cartItem->setProductId($product->getId());
               $cartItem->setQty($request->get('qty'));
               $cartItem->setSize($request->get('size'));
               $cartItem->setUnitPrice($product->getPrice());
               $cartItem->setCreatedAt($now);
               $cartItem->setModifiedAt($now);
               $cartItem->setCart($cart);
               $cartItem->setProduct($product);
           }
           
           $em->persist($cartItem);
           $em->flush();
           
           //product add to cart go to home Jesus you know my heart - Jesus my saviour and Lord
           return $this->redirectToRoute('home');
    }

    /**
     * @Route("/prod-detail/{product_id}", name="product_detail")
     */
    public function productDetailAction(Request $request, $product_id)
    {       
           //used for setting session id in cart entity    
           $session = $request->getSession();
           $session->start();   
                  
           $em = $this->getDoctrine()->getManager();
          
           //grap the selected product for add cart
           $product = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $product_id));
           
           $category = array();  //Thank you lord thank you fathe thankyou holy spirit help me to be partof working for your kingdom but I don't know what I ask
           $category = $product->getCategory();
           
           // can be accessed $category->getTitle());
           
           
           //grap shopping cart for side view of the current cart 
           $items = array();
           $total = 0;
           
           //lets grap logged in user if available 
           $user = $this->get('security.token_storage')->getToken()->getUser();
           
           if($user == 'anon.')   //anonymouse login
           {
               $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
               //grap the cart for current user  - by session id  
               $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           else {
              $user = $this->getUser();
              //grap the cart for current user  - by userId
              $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           
           if($cart)
           {
              $items = $cart->getItems();
              $total = 0;
         
              foreach($items as $item)
              { 
                 $total += floatval($item->getQty() * $item->getUnitPrice());
              }   
           }
          
           //grap the product size of the selected product 
           $product_size = $em->getRepository('AppBundle:ProductSize')->findBy(array('productId' => $product_id));
           
           return $this->render('cart/add_cart.html.twig', array(
              'product' => $product,
              'product_size' => $product_size,
              'items' => $items,
              'total' => $total,
              'category' => $category,
           ));
    }
    
    /**
     * @Route("/show-cart", name="show_cart")
     */
     public function showCartAction(Request $request)
     {
           //used for setting session id in cart entity    
           $session = $request->getSession();
           $session->start();   
                   
           $em = $this->getDoctrine()->getManager();   
           
           //lets grap logged in user if available 
           $user = $this->get('security.token_storage')->getToken()->getUser();
           if($user == 'anon.')   //anonymouse login
           {
               $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
               //grap the cart for current user  - by session id  
               $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
           }
           else {
              $user = $this->getUser();
              //grap the cart for current user  - by userId
              $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           }

           $items = $cart->getItems();
           $total = 0;
           
           foreach($items as $item)
           { 
               $total += floatval($item->getQty() * $item->getUnitPrice());
           }
           
           return $this->render('cart/show_cart.html.twig', array(
            'user' => $user,
            'items' => $items,
            'total' => $total,
           ));
     }

     /**
      * @Route("/update", name="update_cart")
      */
      public function updateCartAction(Request $request)
      {
          $items = $request->get('item');
          
          $em = $this->getDoctrine()->getManager();
          
          //$key = cartItem id $value = udated qty 
          foreach ($items as $key => $value) {
              $cartItem = $em->getRepository('AppBundle:CartItem')->find($key);
              if(intval($value) > 0)
              {
                  $cartItem->setQty($value);
                  $em->persist($cartItem);
              } else {
                  $em->remove($cartItem);
              }
          }
          //persist to dataase 
          $em->flush();
              
          $this->addFlash('success', 'cart updated');
              
          return $this->redirectToRoute('show_cart');
         
      }
      
}
