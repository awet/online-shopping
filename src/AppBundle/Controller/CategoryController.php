<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ImageUploader;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Category controller.
 *
 * @Route("category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/", name="category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppBundle:Category')->findAll();
        
        return $this->render('category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/new", name="category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('AppBundle\Form\CategoryType', $category, array(
               'allow_edit_image' => true,
               'allow_edit_field' => true,
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($image = $category->getImage()){
             
               // $file stores the uploaded PDF file
               /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
               $file = $category->getImage();
               
               // Generate a unique name for the file before saving it
               $fileName = md5(uniqid()).'.'.$file->guessExtension();
               
               // Move the file to the directory where image saved (web/uploads/img)
               $file->move(
                   $this->getParameter('images_dir'),
                   $fileName
               );
               
               
               // set the image 
               $category->setImage($fileName);
             
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_show', array('id' => $category->getId()));
        }

        return $this->render('category/new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="category_show")
     * @Method("GET")
     */
    public function showAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('category/show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="category_edit_data")
     * @Method({"GET", "POST"})
     */
    public function editDataAction(Request $request, Category $category)
    {
            
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('AppBundle\Form\CategoryType', $category, array(
                 'allow_edit_image' => false,
        ));

        // for the category fields update - with out the category image  
        $imageForm = $this->createForm('AppBundle\Form\CategoryType', $category, array(
               'allow_edit_image' => true,
               'allow_edit_field' => false,
               'action' => $this->generateUrl('category_edit_image', array('id' => $category->getId())),
               'method' => 'PATCH',
         ));
                       
        $editForm->handleRequest($request);
        
        if ($editForm->isSubmitted() && 
            $editForm->get('title')->isValid() && 
            $editForm->get('urlKey')->isValid() &&
            $editForm->get('description')->isValid()
        
        ) {                    
            $this->getDoctrine()->getManager()->flush();
            
            // lets add the flash message 
            $request->getSession()
                    ->getFlashBag()
                    ->add('success', ' Category successfully updated ! ' . $category->getId() );
                  
            return $this->redirectToRoute('category_edit_data', array('id' => $category->getId()));
        }
       
        return $this->render('category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'image_form' => $imageForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="category_edit_image")
     * @Method({"PATCH"})
     */
    public function editImageAction(Request $request, Category $category)
    {
            
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('AppBundle\Form\CategoryType', $category, array(
                 'allow_edit_image' => false,
        ));

        // for category image update   
        $imageForm = $this->createForm('AppBundle\Form\CategoryType', $category, array(
               'allow_edit_image' => true,
               'allow_edit_field' => false,
               'action' => $this->generateUrl('category_edit_image', array('id' => $category->getId())),
               'method' => 'PATCH',
         ));
                       
        $imageForm->handleRequest($request);

        if ($imageForm->isSubmitted() && $imageForm->get('image')->isValid()) {
            if($image = $category->getImage()){
             
               // $file stores the uploaded PDF file
               /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
               $file = $category->getImage();
               
               // Generate a unique name for the file before saving it
               $fileName = md5(uniqid()).'.'.$file->guessExtension();
               
               // Move the file to the directory where image saved )
               $file->move(
                   $this->getParameter('images_dir'),
                   $fileName
               );
               
               // Update the file name
               // instead of its contents
               $category->setImage($fileName);
             
            }            
                                    
            $this->getDoctrine()->getManager()->flush();
            
            // lets add the flash message 
            $request->getSession()
                    ->getFlashBag()
                    ->add('success', ' Category image successfully updated ! CATEGORY CODE: ' . $category->getId() );
                  
            return $this->redirectToRoute('category_edit_data', array('id' => $category->getId()));
        }
       
        return $this->render('category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'image_form' => $imageForm->createView(),
        ));
    }


    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
