<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Product controller.
 *
 * @Route("terms")
 */
class TermsController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/return-policy", name="return_policy")
     * @Method("GET")
     */
    public function indexAction()
    {
        $terms = "";
        return $this->render('terms/return_policy.html.twig', array(
            'terms' => $terms,
        ));
    }
    
    /**
     * Lists all product entities.
     *
     * @Route("/faq", name="faq")
     * @Method("GET")
     */
    public function faqAction()
    {
        $terms = "";
        return $this->render('terms/faq.html.twig', array(
            'terms' => $terms,
        ));
    }

    /**
     * Lists all product entities.
     *
     * @Route("/privacy-policy", name="privacy_policy")
     * @Method("GET")
     */
    public function privacyAction()
    {
        $terms = "";
        return $this->render('terms/privacy_policy.html.twig', array(
            'terms' => $terms,
        ));
    } 
    
    /**
     * Lists all product entities.
     *
     * @Route("/terms-and-conditions", name="terms_and_conditions")
     * @Method("GET")
     */
    public function termsAndConditionsAction()
    {
        $terms = "";
        return $this->render('terms/terms_and_conditions.html.twig', array(
            'terms' => $terms,
        ));
    }        
}