<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductSize;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Product controller.
 *
 * @Route("product")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="product_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('AppBundle:Product')->findAll();

        return $this->render('product/index.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * Jesus is the secret.
     *
     * @Route("/best-products/{page}", name="best_products")
     * @Method({"GET"})
     */
    public function bestProducts($page)
    {     
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $dql ="SELECT p FROM AppBundle\Entity\Product p";
        
        $query = $em->createQuery($dql);
                        
        //$result = $this->em->getRepository
                // ('AppBundle:Product')->findBy(array('onsale'  => false), null, 20);
                 
        $limit = 20;
        $thisPage = $page;  
        $paginator = $this->paginate($query, $page, $limit);
        
        $maxPages = ceil($paginator->count() / $limit);
        
        foreach($paginator as $row)
        {
           //insert in the declared array Lord Thank You
           $short_title = (strlen($row->getTitle()) > 41) ? substr($row->getTitle(),0,41).'...' : $row->getTitle();
           $data[] = array(
                  'path' => $this->generateUrl('product_detail', array('product_id' => $row->getId())),
                  'name' => $short_title,
                  'img' => $row->getImage(),
                  'price' => $row->getPrice(),
                  'add_to_cart_url' => $row->getUrlKey(),
                  'id' => $row->getId(),
           );
            
        }
        
        return $this->render('product/best_products.html.twig', array(
            'best_products' => $data,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage,
        ));      
        
    }   

    public function paginate($dql, $page =1 , $limit)
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
                  ->setFirstResult($limit * ($page -1))
                  ->setMaxResults($limit);
       
       return $paginator;
    }

    /**
     * Jesus is the secret.
     *
     * @Route("/products-on-sale/{page}", name="products_on_sale")
     * @Method({"GET"})
     */
    public function productsOnSale($page)
    {     
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $dql ="SELECT p FROM AppBundle\Entity\Product p WHERE p.onsale = 'false'";
        
        $query = $em->createQuery($dql);
                        
        //$result = $this->em->getRepository
                // ('AppBundle:Product')->findBy(array('onsale'  => false), null, 20);
                 
        $limit = 5;
        $thisPage = $page;  
        $paginator = $this->paginate($query, $page, $limit);
        
        $maxPages = ceil($paginator->count() / $limit);
        
        foreach($paginator as $row)
        {
           //insert in the declared array Lord Thank You
           $short_title = (strlen($row->getTitle()) > 41) ? substr($row->getTitle(),0,41).'...' : $row->getTitle();
           $data[] = array(
                  'path' => $this->generateUrl('product_detail', array('product_id' => $row->getId())),
                  'name' => $short_title,
                  'img' => $row->getImage(),
                  'price' => $row->getPrice(),
                  'add_to_cart_url' => $row->getUrlKey(),
                  'id' => $row->getId(),
           );
            
        }
        
        return $this->render('product/products_on_sale.html.twig', array(
            'products_on_sale' => $data,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage,
        ));      
        
    }   


    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="product_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('AppBundle\Form\ProductType', $product, array(
               'allow_edit_image' => true,
        ));        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($image = $product->getImage()){
             
               // $file stores the uploaded PDF file
               /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
               $file = $product->getImage();
               
               // Generate a unique name for the file before saving it
               $fileName = md5(uniqid()).'.'.$file->guessExtension();
               
               // Move the file to the directory where image saved (app/uploads/category_images)
               $file->move(
                   $this->getParameter('images_dir'),
                   $fileName
               );
               
               // Update the file name
               // instead of its contents
               $product->setImage($fileName);
             
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            // lets add the flash message 
            $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Product created successfully !');
                    
            return $this->redirectToRoute('product_show', array('id' => $product->getId()));
        }

        return $this->render('product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="product_show")
     * @Method("GET")
     */
    public function showAction(Product $product)
    {
      
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/size/{id}", name="product_size_show")
     * @Method("GET")
     */
    public function showProductSizeAction($id)
    {
            
        $data = array();
        $em = $this->getDoctrine()->getManager();
        
        $product = $em->getRepository('AppBundle:Product')->findOneBy(array('id' => $id));
        
        
        $product_size = $em->getRepository('AppBundle:ProductSize')->findBy(array('productId'  => $id), null, 20);    
            
        return $this->render('product/show_size.html.twig', array(
            'product' => $product,
            'productSize' => $product_size,
        ));
    }

   /**
     * Displays a form to edit an existing product entity - with out updating the picture .
     *
     * @Route("/{id}/edit", name="product_edit_data")
     * @Method({"GET", "POST"})
     */
    public function editDataAction(Request $request, Product $product)
    {
        
        $deleteForm = $this->createDeleteForm($product);
        
        // allow_edit_image = false will exclude the image field it is set in the form builder 
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product, array(
               'allow_edit_image' => false,
        ));
        
        // for the image update  
        $imageForm = $this->createForm('AppBundle\Form\ProductType', $product, array(
               'allow_edit_image' => true,
               'allow_edit_field' => false,
               'action' => $this->generateUrl('product_edit_image', array('id' => $product->getId())),
               'method' => 'PATCH',
        ));
              
        $editForm->handleRequest($request);
        
        //for fields sumbmitted
        if ($editForm->isSubmitted() && 
            $editForm->get('title')->isValid() && 
            $editForm->get('sku')->isValid() &&
            $editForm->get('urlKey')->isValid() &&
            $editForm->get('description')->isValid() &&
            $editForm->get('qty')->isValid() &&
            $editForm->get('category')->isValid() &&
            $editForm->get('onsale')->isValid()
        ) 
        {                     
               
            $this->getDoctrine()->getManager()->flush();
            
            // lets add the flash message 
            $request->getSession()
                    ->getFlashBag()
                    ->add('success', ' Product successfully updated ! ' . $product->getId() );
                  
            return $this->redirectToRoute('product_edit_data', array('id' => $product->getId()));
        }
        
        //dump($product);
        //exit;
        
        return $this->render('product/edit_data.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'image_form' => $imageForm->createView(),
            
        ));
    }

   /**
     * updating the picture only and setting other form fields.
     *
     * @Route("/{id}/edit", name="product_edit_image")
     * @Method({"PATCH"})
     */
    public function editImageAction(Request $request, Product $product)
    {
                
        $deleteForm = $this->createDeleteForm($product);
        
        // allow_edit_image = false will exclude the image field it is set in the form builder 
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product, array(
               'allow_edit_image' => false,
        ));
        
        // for the image update  
        $imageForm = $this->createForm('AppBundle\Form\ProductType', $product, array(
               'allow_edit_image' => true,
               'allow_edit_field' => false,
               'action' => $this->generateUrl('product_edit_image', array('id' => $product->getId())),
               'method' => 'PATCH',
        ));
        
        $imageForm->handleRequest($request);
        
        //for image sumbmitted
        if ($imageForm->isSubmitted() && $imageForm->get('image')->isValid()
        ) 
        {
            
                                     
            if($image = $product->getImage()){
             
               // $file stores the uploaded PDF file
               /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
               $file = $product->getImage();
               
               // Generate a unique name for the file before saving it
               $fileName = md5(uniqid()).'.'.$file->guessExtension();
               
               // Move the file to the directory where image saved (app/uploads/category_images)
               $file->move(
                   $this->getParameter('images_dir'),
                   $fileName
               );
               
               // Update the file name
               // instead of its contents
               $product->setImage($fileName);
             
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            // lets add the flash message 
            $request->getSession()
                    ->getFlashBag()
                    ->add('success', ' Product image successfully updated ! PRODUCT CODE: ' . $product->getId() );
                  
            return $this->redirectToRoute('product_edit_data', array('id' => $product->getId()));
        }        
        return $this->render('product/edit_data.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'image_form' => $imageForm->createView(),
            
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Find and displays product by category.
     *
     * @Route("/category/{id}", name="product_by_category_show")
     * @Method("GET")
     */
    public function productByCategory($id)
    {
        $data = array();
        $em = $this->getDoctrine()->getManager();
        
        $category = $em->getRepository('AppBundle:Category')->findOneBy(array('id'  => $id), null, 20);
        
        $products = $em->getRepository
                 ('AppBundle:Product')->findBy(array('category'  => $id), null, 20);
                
        foreach($products as $row)
        {
           //insert in the declared array Lord Thank You
           $short_title = (strlen($row->getTitle()) > 41) ? substr($row->getTitle(),0,41).'...' : $row->getTitle();
           $data[] = array(
                  'path' => $this->generateUrl('product_detail', array('product_id' => $row->getId())),
                  'name' => $short_title,
                  'img' => $row->getImage(),
                  'price' => $row->getPrice(),
                  'add_to_cart_url' => $row->getUrlKey(),
                  'id' => $row->getId(),
           );
            
        }
 
        return $this->render('product/show_by_category.html.twig', array(
            'products' => $data, 
            'category_title' => $category->getTitle(),
        ));       
             
        
    }
    
    
}
