<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BillingShipping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Billingshipping controller.
 *
 * @Route("billingshipping")
 */
class BillingShippingController extends Controller
{
    /**
     * Lists all billingShipping entities.
     *
     * @Route("/", name="billingshipping_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $billingShippings = $em->getRepository('AppBundle:BillingShipping')->findAll();

        return $this->render('billingshipping/index.html.twig', array(
            'billingShippings' => $billingShippings,
        ));
    }

    /**
     * Creates a new billingShipping entity.
     *
     * @Route("/billing-shipping", name="billingshipping_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
             
        //used for setting session id in cart entity    
        $session = $request->getSession();
        $session->start();   
         
        $em = $this->getDoctrine()->getManager();  
        //lets grap logged in user if available 
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user == 'anon.')   //anonymouse login
        {
            $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
            //grap the cart for current user  - by session id  
            $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
        }
        else {
           $user = $this->getUser();
           //grap the cart for current user  - by userId
           $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
        }  
        
        if($cart){
           $items = $cart->getItems();
           $total = 0;
             
           foreach($items as $item)
           { 
              $total += floatval($item->getQty() * $item->getUnitPrice());
           }     
               
           
           //let grap billingShipping for the cart if it exits 
           $exiting_billingShipping = $em->getRepository('AppBundle:BillingShipping')->findOneBy(array('cartId' => $cart->getId()));
               
           $billingShipping = new Billingshipping();
           
           $form = $this->createForm('AppBundle\Form\BillingShippingType', $billingShipping);
           $form->handleRequest($request);
   
           if ($form->isSubmitted() && $form->isValid()) {
               //THIS IS THE PROPER WAY TO ACCESS SYMFONY FORM PARAMETER    
               //dump($request->request->get('appbundle_billingshipping')['country']); //THANK YOU LORD 
               
               //Setting shipping rate based on country code 
               if($request->request->get('appbundle_billingshipping')['country'] == $this->getParameter('flat_rate_shipping_country_code'))
               {
                   $billingShipping->setShippingRate(floatval($this->getParameter('shipping_rate_1')));  //this is zero on the time of coding. free for ethiopian shippments
               } else {
                   $billingShipping->setShippingRate(floatval($this->getParameter('shipping_rate_2'))); 
               }
                
               //let check if the billing shipping is new 
               if($exiting_billingShipping) //update the exiting billingShipping  
               {
                   //beacuse we need to edit the exiting data give to the form the exiting entity and then handle the form request finally flush     
                   $form = $this->createForm('AppBundle\Form\BillingShippingType', $exiting_billingShipping);
                   $form->handleRequest($request);    
                   
                   $this->getDoctrine()->getManager()->flush();
                   
               } else {  //create new billingShipping 
                //FK one to one relation
                $billingShipping->setCartId($cart->getId());
                $em = $this->getDoctrine()->getManager();
                $em->persist($billingShipping);
                $em->flush();   
               }
               
               if($this->getParameter('payment_gateway_type')=="2CO")
               {
                  return $this->render('gateway/payment_2.html.twig', array(
                                  'sid' => '901362471',
                                  'mode' => '2CO',
                                  'items' => $items,
                                  'total' => $total,
                                  
                  ));
               }
               elseif($this->getParameter('payment_gateway_type')=="2CO-API")
               {
                  //2checkout payment gateway    
                  return $this->render('gateway/payment_3.html.twig', array(
                                  'sid' => '901362471',
                                  'mode' => '2CO',
                                  'items' => $items,
                                  'total' => $total,         
                  ));
                                                      
                                                      
               } else {  //braintree
                   return $this->redirectToRoute('payment_method', array('id' => $billingShipping->getId()));
               }
               
           }
   
           return $this->render('billingshipping/billingShipping.html.twig', array(
               'billingShipping' => $billingShipping,
               'form' => $form->createView(),
               'items' => $items,
               'total' => $total,
           ));          
        } //end cart item if
        
        // if no no cart item  
        $request->getSession()
                ->getFlashBag()
                ->add('success', 'Please add items to your cart ! ');
       
        return $this->redirectToRoute('home');                                        
    }

     /**
      * @Route("/shipping-rate", name="shipping-rate")  
      * @Method("POST")  
      */
     public function calculateShippingRate(Request $request)
     {

        //used for setting session id in cart entity    
        $session = $request->getSession();
        $session->start();   
         
        $em = $this->getDoctrine()->getManager();  
        //lets grap logged in user if available 
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($user == 'anon.')   //anonymouse login
        {
            $user =  $em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
            //grap the cart for current user  - by session id  
            $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
        }
        else {
           $user = $this->getUser();
           //grap the cart for current user  - by userId
           $cart = $em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
        }  
        
        $items = $cart->getItems();
        $total = 0;
          
        foreach($items as $item)
        { 
           $total += floatval($item->getQty() * $item->getUnitPrice());
        } 
        
        //lets calculate the shipping rate based on country code 
        $grand_total=0;
        $shipping_rate=0;
        if($request->get('par_1')==$this->getParameter('flat_rate_shipping_country_code'))
        {
            $shipping_rate = floatval($this->getParameter('shipping_rate_1'));
        }else{
            $shipping_rate = floatval($this->getParameter('shipping_rate_2'));
        } 
        
        $grand_total = $total + $shipping_rate;     
        
        $rate = array('subtotal' => $total, 
                      'shipping_rate' => $shipping_rate,
                      'grand_total' => $grand_total,
                      
         );
             
        return new JsonResponse($rate);   
     }


    /**
     * Finds and displays a billingShipping entity.
     *
     * @Route("/{id}", name="billingshipping_show")
     * @Method("GET")
     */
    public function showAction(BillingShipping $billingShipping)
    {
        $deleteForm = $this->createDeleteForm($billingShipping);

        return $this->render('billingshipping/show.html.twig', array(
            'billingShipping' => $billingShipping,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing billingShipping entity.
     *
     * @Route("/{id}/edit", name="billingshipping_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BillingShipping $billingShipping)
    {
        $deleteForm = $this->createDeleteForm($billingShipping);
        $editForm = $this->createForm('AppBundle\Form\BillingShippingType', $billingShipping);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('billingshipping_edit', array('id' => $billingShipping->getId()));
        }

        return $this->render('billingshipping/edit.html.twig', array(
            'billingShipping' => $billingShipping,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a billingShipping entity.
     *
     * @Route("/{id}", name="billingshipping_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BillingShipping $billingShipping)
    {
        $form = $this->createDeleteForm($billingShipping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($billingShipping);
            $em->flush();
        }

        return $this->redirectToRoute('billingshipping_index');
    }

    /**
     * Creates a form to delete a billingShipping entity.
     *
     * @param BillingShipping $billingShipping The billingShipping entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BillingShipping $billingShipping)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('billingshipping_delete', array('id' => $billingShipping->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
