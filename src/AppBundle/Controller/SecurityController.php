<?php

namespace AppBundle\Controller;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        //get login error if there is one
        $error = $authUtils->getLastAuthenticationError(); 
       
       //last user name entered 
       $lastUserName = $authUtils->getLastUsername();
       
         
        return $this->render('security/login.html.twig', array(
              'last_username' => $lastUserName,
              'error'         => $error,
            
        ));
    }
    
   /**
    * @Route("/logout", name="logout")
    */
    public function logoutAction(Request $request)
    {
        $token = new AnonymousToken($providerKey, 'anon.');
        $this->get('security.context')->setToken($token);
        $this->get('request')->getSession()->invalidate();
    }

}
