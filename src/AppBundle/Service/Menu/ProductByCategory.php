<?php

namespace AppBundle\Service\Menu;

class ProductByCategory {
        
    public function __construct(
       \Doctrine\ORM\EntityManager $entityManager,
       \Symfony\Bundle\FrameworkBundle\Routing\Router $router
    )
    {
        $this->em = $entityManager;
        $this->router = $router;
    }
        
    public function getItems($category)
    {
        $data = array();
        $result = $this->em->getRepository
                 ('AppBundle:Product')->findBy(array('category'  => $category), null, 20);
        foreach($result as $row)
        {
           //insert in the declared array Lord Thank You
           $short_title = (strlen($row->getTitle()) > 41) ? substr($row->getTitle(),0,41).'...' : $row->getTitle();
           $data[] = array(
                  'path' => $this->router->generate('product_detail', array('product_id' => $row->getId())),
                  'name' => $short_title,
                  'img' => $row->getImage(),
                  'price' => $row->getPrice(),
                  'add_to_cart_url' => $row->getUrlKey(),
                  'id' => $row->getId(),
           );
            
        }
        
        return $data;
    }   
        
    
   
    
}
