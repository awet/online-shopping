<?php

namespace AppBundle\Service\Menu;

class Category 
{
    private $em;
    private $router;
    
    public function __construct(
        \Doctrine\ORM\EntityManager $entityManager,
        \Symfony\Bundle\FrameworkBundle\Routing\Router $router
    )
    {
        $this->em = $entityManager;
        $this->router = $router;
    }
    
    public function getItems()
    {
       $categories = array();
       $_categories = $this->em->getRepository('AppBundle:Category')->findAll();
       
       foreach($_categories as $_category)
       {
           $categories[] = array(
             'path' => $this->router->generate('product_by_category_show', array('id' => $_category->getId())),
             'label' => $_category->getTitle(),
           );
       }    
       
       return $categories;
    }
    
    public function getItems_old()
    {
        return array(
          array('path' => 'women', 'label' => 'Women'),
          array('path' => 'men', 'label' => 'Men'),
          array('path' => 'sport', 'label' => 'Sport'),
        );
    }
    
}
