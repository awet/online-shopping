<?php

namespace AppBundle\Service\Menu;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BestSellers {
        
    public function __construct(
       \Doctrine\ORM\EntityManager $entityManager,
       \Symfony\Bundle\FrameworkBundle\Routing\Router $router
    )
    {
        $this->em = $entityManager;
        $this->router = $router;
    }
        
    public function getItems($currentPage = 1)
    {     
        $data = array();
        $dql ="SELECT p FROM AppBundle\Entity\Product p";
        
        $query = $this->em->createQuery($dql);
                        
        //$result = $this->em->getRepository
                // ('AppBundle:Product')->findBy(array('onsale'  => false), null, 20);
                 
        $limit = 3;
        $thisPage = $currentPage;  
        $paginator = $this->paginate($query, $currentPage, $limit);
        
        $maxPages = ceil($paginator->count() / $limit);
        
        foreach($paginator as $row)
        {
           //insert in the declared array Lord Thank You
           $short_title = (strlen($row->getTitle()) > 41) ? substr($row->getTitle(),0,41).'...' : $row->getTitle();
           $data[] = array(
                  'path' => $this->router->generate('product_detail', array('product_id' => $row->getId())),
                  'name' => $short_title,
                  'img' => $row->getImage(),
                  'price' => $row->getPrice(),
                  'add_to_cart_url' => $row->getUrlKey(),
                  'id' => $row->getId(),
           );
            
        }
        
        return $data;
    }   
        
    public function paginate($dql, $page =1 , $limit)
    {
        $paginator = new Paginator($dql);
        $paginator->getQuery()
                  ->setFirstResult($limit * ($page -1))
                  ->setMaxResults($limit);
       
       return $paginator;
    }
   
    
}
