<?php

namespace AppBundle\Service\Menu;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class Checkout {
    
    private $em;
    private $token;
    private $router;
    private $requestStack; 
    
    public function __construct(
      \Doctrine\ORM\EntityManager $entityManager,
      $tokenStorage,
      \Symfony\Bundle\FrameworkBundle\Routing\Router $router,
       RequestStack $requestStack
    )
    {
       $this->em = $entityManager;
       $this->token = $tokenStorage;
       $this->router = $router; 
       $this->requestStack = $requestStack;
    }  
    
    public function getCartCount()
    {
      //used for setting session id in cart entity  
      $request = $this->requestStack->getCurrentRequest();  
      $session = $request->getSession();
      $session->start();   
          
      $cartCount = 0;
      $user = $this->token->getToken()->getUser();
     
      //lets grap logged in user if available 
      if($user == 'anon.')   //anonymouse login
      {
          $user =  $this->em->getRepository('AppBundle:User')->findOneBy(array('email' => 'guest@awetshop.com'));   //my Lord Thank You I am so happy !! :-)  
          //grap the cart for current user  - by session id  
          $cart = $this->em->getRepository('AppBundle:Cart')->findOneBy(array('sessionId' => $session->getId()));   //my Lord Thank You I am so happy !! :-)
      }
      else {
         //$user = $this->getUser();
         //grap the cart for current user  - by userId
         $cart = $this->em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
      }
      
      if($cart)
      {
          $cartCount = $cart->countItems();
      }
      
      return $cartCount;  
    }
    
    public function getItems()
    {

       $items = array();  
       $user = $this->token->getToken()->getUser();
       if($user instanceof User)
       {
           
           $cart = $this->em->getRepository('AppBundle:Cart')->findOneBy(array('userId' => $user->getId()));   //my Lord Thank You I am so happy !! :-)
           if($cart)
           {
                  
              $items[] = array(
                               'path' => 'cart',
                               'lable' => $cart->countItems(), 
                         );
              $items[] = array(
                                'path' => 'checkout',
                                'lable' => 'Checkout', 
                         );              
           } else {
               
           }
               
           
           
           return $items;
       }
    }  
    
    public function getItems_old()
    {
        //intial dummy menu
        return array(
          array('path' => 'cart', 'lable' => 'Cart (3)'),
          array('path' => 'checkout', 'lable' => 'Checkout'),
        );
    }
}
