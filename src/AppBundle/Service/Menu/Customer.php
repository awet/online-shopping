<?php

namespace AppBundle\Service\Menu;

use AppBundle\Entity\User;

class Customer 
{
    private $token;
    private $router;
    
    public function __construct(
      $tokenStorage,
      \Symfony\Bundle\FrameworkBundle\Routing\Router $router
    )
    {
       $this->token = $tokenStorage;
       $this->router = $router; 
    }
    
    public function getItems()
    {
        $items = array();
        $user = $this->token->getToken()->getUser();
        
        if($user instanceof User)
        {
            //authentication
            $items[] = array(
                             'path' => $this->router->generate('home'),
                             'lable' => 'shalom ' . $user->getFirstName() . ' !', 
                       );
            $items[] = array(
                             'path' => $this->router->generate('logout'),
                             'lable' => 'Logout', 
                       );           
        } else {
            $items[] = array(
                             'path' => $this->router->generate('home'),
                             'lable' => 'Hello shopper !', 
                       );            
           $items[] = array(
                             'path' => $this->router->generate('user_registration'),
                             'lable' => 'Register', 
                       );
           $items[] = array(
                             'path' => $this->router->generate('login'),
                             'lable' => 'Login', 
                       );
        }
        
        return $items;
    }
    
    public function getItems2()
    {
        return array(
          array('path' => 'account', 'label' => 'Awet Tsegazeab'),
          array('path' => 'logout', 'label' => 'Logout'),
        );
    }
    
}
