<?php

namespace AppBundle\Service\Menu;

class OnSale 
{
    private $em;
    private $router;
    
    public function __construct(
      \Doctrine\ORM\EntityManager $entityManager,
      \Symfony\Bundle\FrameworkBundle\Routing\Router $router
    )
    {
      $this->em = $entityManager;
      $this->router = $router;   
    }
    
    public function getItems()
    {
       $products = array();
       $_products = $this->em->getRepository
                    ('AppBundle:Product')->findBy(array('onsale' => false), null, 5);
       
       foreach($_products as $_product)
       {
           //insert in the declared array Lord Thank You
           $products[] = array(
                  'path' => $this->router->generate('product_detail', array('product_id' => $_product->getId())),
                  'name' => $_product->getTitle(),
                  'img' => $_product->getImage(),
                  'price' => $_product->getPrice(),
                  'add_to_cart_url' => $_product->getUrlKey(),
                  'id' => $_product->getId(),
           );
       }  
       
       return $products;           
       
    }
    
    public function getItems2()
    {
        //this dummy data
        return array(
        
          array('path' => 'iphone', 'name' => 'iphone', 'img' => 'https://placehold.it/200x200', 'price' => 19.41, 'add_to_cart_url' => '#'),
          array('path' => 'iphon', 'name' => 'Japan Movement', 'img' => 'https://placehold.it/200x200', 'price' => 10.50, 'add_to_cart_url' => '#'),
          array('path' => 'samsung', 'name' => 'Samsung', 'img' => 'https://placehold.it/200x200', 'price' => 51.50, 'add_to_cart_url' => '#'),
          array('path' => 'sony', 'name' => 'Sony', 'img' => 'https://placehold.it/200x200', 'price' => 2,050.50, 'add_to_cart_url' => '#'),
          array('path' => 'iphone', 'name' => 'iphone', 'img' => 'https://placehold.it/200x200', 'price' => 19.41, 'add_to_cart_url' => '#'),
          
        );
    }
    
}
