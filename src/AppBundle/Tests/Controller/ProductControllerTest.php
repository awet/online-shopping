<?php

namespace AppBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase {
    
    public function testProductCreate()
    {
        //cleint to browse the application     
        $client = static::createClient();
        
        $crawler = $client->request('POST', '/product/new');
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /product/");
        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'appbundle_product[title]'  => 'Test product title',
            'appbundle_product[price]'  => floatval(100),
            'appbundle_product[sku]'  => 'Test description',
            'appbundle_product[urlKey]'  => 'test image',
            'appbundle_product[description]'  => 'test image',
            'appbundle_product[qty]'  => floatval(10),
            'appbundle_product[category]'  => '1',
            'appbundle_product[onsale]'  => 1,
            'appbundle_product[image]'  => 'image'
            // ... other fields to fill
        ));
        $client->submit($form);
        
    }
    
} 