<?php

namespace AppBundle\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryControllerTest extends WebTestCase {
    
    public function testCategoryCreate()
    {
        //cleint to browse the application     
        $client = static::createClient();
        
        $crawler = $client->request('GET', '/category/new');
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /category/");
        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'appbundle_category[title]'  => 'Test Category title',
            'appbundle_category[urlKey]'  => 'Test url key',
            'appbundle_category[description]'  => 'Test description',
            'appbundle_category[image]'  => 'test image',
            // ... other fields to fill
        ));
        $client->submit($form);
        
    }
    
} 