<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 */
class Product
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $categoryId;
    
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var string
     */
    private $urlKey;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $qty;

    /**
     * @var boolean
     */
     private $onsale;
     
    /**
     * @var string
     * 
     * @ORM\Column(name="image", type="string", length=255,
       nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" },
       mimeTypesMessage="Please upload the PNG or JPEG image file.")
     */
    private $image;
    
    private $cartItems;   //FK
    
    private $productSize;   //FK for ProductSize entity
    
    private $category;
    
    public function __construct()
    {
        $this->cartItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productSize = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }
    
    public function getCategory()
    {
        return $this->category;
    }
    
    public function addCartItem(CartItem $cartItems)
    {
        $this->cartItems->add($cartItems);
        $cartItems->setProduct($this);
    }
    
    public function getCartItems()
    {
        return $this->cartItems;
    }
 
    public function addProductSize(ProductSize $productSize)
    {
        $this->productSize->add($productSize);
        $productSize->setProduct($this);    //this is found in Entity ProductSize 
    }
    
    public function getProductSize()
    {
        return $this->productSize;
    }
       
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set urlKey
     *
     * @param string $urlKey
     *
     * @return Product
     */
    public function setUrlKey($urlKey)
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    /**
     * Get urlKey
     *
     * @return string
     */
    public function getUrlKey()
    {
        return $this->urlKey;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return Product
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return Product
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }
    
    /**
     * Get categoryId
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }
    /**
     * Set image
     *
     * @param string $image
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    
    /**
     * Set onsale
     *
     * @param boolean $onsale
     *
     * @return Product
     */
    public function setOnsale($onsale)
    {
        $this->onsale = $onsale;

        return $this;
    }

    /**
     * Get onsale
     *
     * @return boolean
     */
    public function getOnsale()
    {
        return $this->onsale;
    }
}
