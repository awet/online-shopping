<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductSize
 */
 
 class ProductSize 
 {
     
     private $id;
     private $productId;
     private $size;
     private $qty;
     private $createdAt;
     private $modifiedAt;
     
     private $product;
    
     public function getId()
     {
        return $this->id;
     }
    
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }
    
    public function getProductId()
    {
        return $this->productId;
    }
    
     public function setSize($size)
     {
         $this->size = $size;
     }
     
     public function getSize()
     {
         return $this->size;
     }
    
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    public function getQty()
    {
        return $this->qty;
    }
     
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }
    
    public function getProduct()
    {
        return $this->product;
    }
    
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
    
    public function __toString()
    {
        return $this->getSize();
    }
 }
