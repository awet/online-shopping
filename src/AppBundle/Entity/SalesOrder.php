<?php

namespace AppBundle\Entity;

/**
 * SalesOrder
 */
class SalesOrder
{
 
    const ORDER_PENDING = "ORDER_PENDING";
    const ORDER_APPROVED = "ORDER_APPROVED";
    const ORDER_SHIPPED = "ORDER_SHIPPED";
    const ORDER_CANCELLED ="ORDER_CANCELLED";
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $userId;
    
    private $billingShippingId;
    
    /**
     * @var string
     */
    private $salesNo;
    
    /**
     * @var string
     */
    private $itemsPrice;

    /**
     * @var string
     */
    private $shipmentPrice;

    /**
     * @var string
     */
    private $totalPrice;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    private $items;
    
    private $billingShipping;
    
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setBillingShipping(BillingShipping $billingShipping)
    {
        $this->billingShipping = $billingShipping;
    }
    
    /**
     * Set setSalesNo
     *
     * @param string $salesNo
     *
     * @return SalesOrder
     */
    public function setSalesNo($salesNo)
    {
        $this->salesNo = $salesNo;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return string
     */
    public function getSalesNo()
    {
        return $this->$salesNo;
    }
    
    
    public function addItem(OrderItem $items)
    {
        $this->items->add($items);
        $items->setOrder($this);
    }
    
    public function getItems()
    {
        return $this->items;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     *
     * @return SalesOrder
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set itemsPrice
     *
     * @param string $itemsPrice
     *
     * @return SalesOrder
     */
    public function setItemsPrice($itemsPrice)
    {
        $this->itemsPrice = $itemsPrice;

        return $this;
    }

    /**
     * Get itemsPrice
     *
     * @return string
     */
    public function getItemsPrice()
    {
        return $this->itemsPrice;
    }

    /**
     * Set shipmentPrice
     *
     * @param string $shipmentPrice
     *
     * @return SalesOrder
     */
    public function setShipmentPrice($shipmentPrice)
    {
        $this->shipmentPrice = $shipmentPrice;

        return $this;
    }

    /**
     * Get shipmentPrice
     *
     * @return string
     */
    public function getShipmentPrice()
    {
        return $this->shipmentPrice;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return SalesOrder
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return SalesOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return SalesOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return SalesOrder
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
}

