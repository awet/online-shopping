<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
             if($options['allow_edit_field']){
               $builder        
               ->add('title')
               ->add('price')
               ->add('sku')
               ->add('urlKey')
               ->add('description')
               ->add('qty')
               ->add('category')
               ->add('onsale'); 
             }
               
             if($options['allow_edit_image'])
             {
                $builder
                ->add('image', FileType::class, array('data_class' => null));
             }
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product',
            'allow_edit_image' => false,
            'allow_edit_field' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }


}
