online shop
===========

online shopping store with easy application and payment gateway. 

## Setup

Make sure your local mysql and apache services are up and running.

run `composer install` 

Then run `php bin/console doctrine:migrations:migrate -n` . This will deploy the db and sample data

## start the system

to start the web run `php bin/console server:start` command from inside online-shopping directory.

## Debugging

Use the `dump` command to printout useful information.

## Routes

All Routes are defined in `src/AppBundle/Controller`. annotations are used in each controller page.

## Model

The model holds all of the business logic and entities for the system.

models are defined in `src/AppBundle/Entity` 

## Commands

All commands are with in `php bin/console` 

## Doctrine

File mappings are yml format and found in `src/AppBundle/Resources/config/doctrine/`.


## Doctrine Migrations

Migration files are located in `app/DoctrineMigrations`

## Payment Gateway

braintree paypal service. 

you can use one of the following testing cards,


4111111111111111 -visa 

378282246310005  -American express

5555555555554444   - MasterCard

configuration for the braintree environment are located `app/config/parameters.yml`



## Payment Gateway testing 

you can test successfull sale transactions by creating sandbox account at 
https://sandbox.braintreegateway.com/ and configuring the following fields based on your braintree account. 
 
 `app/config/parameters.yml`
 
 braintree_env: 
 
 braintree_merchantId: 
 
 braintree_pk: 
 
 braintree_pvk: 

## Other

Perform DQL Queries

```php
<?php

$query = $em->createQuery('SELECT user FROM AppBundle:User user');
$query->getResult();
```

Use an entity repo

```php
<?php

$repo = $em->getRepository('AppBundle:User');
$report = $repo->findOneBy([
    'name' => $name
]);
```

Persist an entity

```php
<?php

$user = new User();
$em->persist($user);

// flush will persist to the db, you only need to call it like once or twice per request
$em->flush();
```

## Tests

test pages are located in `src/AppBundle/Tests`

tests can be run using `./vendor/bin/phpunit`


A Symfony project created on June 19, 2017, 11:31 am.




Awet Tsegazeab
`avvvet@gmail.com`