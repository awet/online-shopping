<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170914081304 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Cycle Force Rigid Mountain Men", "170.99", "QW1", "WWW.1.COM", "Cycle Force Rigid Mountain Men", 10, "cycle.jpg", 3)');
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Lauren 1/2 Size Nylon String Student Guitar - Natural", "150", "QW2", "WWW.2.COM", "Cycle Force Rigid Mountain Men", 10, "guitar.jpg", 1)');
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Guess Rugged Fabric Chronograph Quartz Watch for Men", "120.25", "QW3", "WWW.3.COM", "Cycle Force Rigid Mountain Men", 10, "watch1.jpg", 2)');
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Tommy Hilfiger Quartz Watch for Men", "1200", "QW4", "WWW.4.COM", "Cycle Force Rigid Mountain Men", 10, "watch2.jpg", 2)');
       
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Granados Upholstered Queen Bed", "200.99", "QW5", "WWW.5.COM", "Cycle Force Rigid Mountain Men", 10, "bed.jpg", 4)');
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Retro Modern Faux Leather Stool", "49.50", "QW6", "WWW.6.COM", "Cycle Force Rigid Mountain Men", 10, "stool.jpg", 4)');
       $this->addSql('INSERT INTO product (title, price, sku, url_key , description, qty, image, category_id) values ("Helen Twin Fabric Bed Frame - Atomic", "120", "QW7", "WWW.7.COM", "Cycle Force Rigid Mountain Men", 10, "bed2.jpg", 4)');
  
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
