<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170914081300 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO category (title, url_key , description, image) values ("Music", "www.1.com", "music", "guitar.jpg")');
        $this->addSql('INSERT INTO category (title, url_key , description, image) values ("Watches", "www.2.com", "Watches", "watch1.jpg")');
        $this->addSql('INSERT INTO category (title, url_key , description, image) values ("Sports", "www.3.com", "Sport", "cycle.jpg")');
        $this->addSql('INSERT INTO category (title, url_key , description, image) values ("Furniture", "www.4.com", "Furniture", "stool.jpg")');  
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
