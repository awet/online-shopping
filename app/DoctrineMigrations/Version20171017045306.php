<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171017045306 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sales_order CHANGE billing_shipping billing_shipping INT NOT NULL');
        $this->addSql('ALTER TABLE sales_order ADD CONSTRAINT FK_36D222E35C07ED8 FOREIGN KEY (billing_shipping) REFERENCES billing_shipping (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_36D222E35C07ED8 ON sales_order (billing_shipping)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sales_order DROP FOREIGN KEY FK_36D222E35C07ED8');
        $this->addSql('DROP INDEX UNIQ_36D222E35C07ED8 ON sales_order');
        $this->addSql('ALTER TABLE sales_order CHANGE billing_shipping billing_shipping VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
    }
}
